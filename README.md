



## Sevensense Coding Assignment

Hey guys! Here is my submission for the coding assignment. It consists of the ROS package `jackal_velocity_publisher`, which basically is a ROS node that publishes control commands (linear velocity and heading derivative) to the ROS network. In order to do that, the node accesses the robot state via the odometry messages.

In order to run the node, you can simply use source the setup file of your catkin workspace and start it with the launch file: `roslaunch jackal_velocity_publisher jackal_velocity_publisher.launch`.

The idea I wanted to implement is as follows: for the first 5 seconds, the robot moves with constant linear velocity, with a randomly picked (but constant) angular velocity. Then, it stops moving and starts turning until its heading matches the direction from the current position until the goal (this is achieved through a P(ID) controller). Finally, with constant heading, the robot is sent towards the goal, and the velocity is regulated with an LQR control law that brings the distance to the goal to zero.

The goal position as well as the controller gains are given in the `config.yml` file and are loaded on the parameter server and subsequently passed to the class object.

As the video shows, the idea of the controller is there, but unfortunately I have not been able to make it work properly. In fact, the final position of the robot is clearly not (5,5) (this is what is set in the video). There are also problems with the heading, which is also off. I have tried to debug this for the whole evening and I have come to to a few conclusions: the robot position given by the odometry seems to be unreliable, meaning that these measurements at times drift away from their actual values. For example (you can see this from the printout of the console in which you launch the node) once the robot arrives at its final destination, the position (5,5) is measured, but this does not match the simulation! Similar things happen with the heading...

I am still unsure how to tackle the problem as I am writing this: I thought that the odom frame should provided the correct state but there is probably something that I am missing. If you have any idea why this is happening that would be great! Hopefully you can at least see the idea and the implementation attempt in the code!

Best,  
Leonardo

PS: I've tried to debug again this morning (I thought there might have been a buffering problem with the odom data, but that seems also not to be the case) unsuccessfully and I have added two more videos to show you the behavior with different initial random motions. 

