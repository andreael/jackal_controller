#pragma once

#include "geometry_msgs/Twist.h"
#include "nav_msgs/Odometry.h"
#include "tf/tf.h"
#include <cmath>

struct ControllerGains {
    double K_LQR_vel = 0.0;

    double K_P_ang = 0.0;
    double K_I_ang = 0.0;
    double K_D_ang = 0.0;
};

class JackalVelocityPublisher {
public:

    /**
     * @brief class constructor.
     * @param node_handle: ROS node handle.
     * @param x_goal: x-coord of the goal position.
     * @param y_goal: y-coord of the goal position.
     * @param controller_gains: controller gains for angular and linear velocities.
     */
    JackalVelocityPublisher(ros::NodeHandle& node_handle, double x_goal, double y_goal, ControllerGains& controller_gains)
    : nh_(node_handle), x_goal_(x_goal), y_goal_(y_goal), controller_gains_(controller_gains), dist_err_sum_(0.0),
    ang_err_sum_(0.0), dist_err_prev_(0.0), ang_err_prev_(0.0), turning_finished_(false), first_call_(true), at_initial_pos_(false)
    {
        setup();
        publishCommands();
    }
private:

    /// methods

    /**
     * @brief sets up the publisher and subscriber objects as well as a few member variables.
     */
    void setup() {
        // publisher
        command_publisher_ = nh_.advertise<geometry_msgs::Twist>("jackal_velocity_controller/cmd_vel", 1000);
        // subscriber
        robot_state_subscriber_ = nh_.subscribe<nav_msgs::Odometry>("jackal_velocity_controller/odom", 1000, &JackalVelocityPublisher::setCommand, this);

        // set random angular speed (-1,1) for the initial motion
        srand(time(NULL));
        random_angular_speed_ = 2 * (( (double)rand() / (double)RAND_MAX )) - 1;

        // set motion start time
        motion_start_time_ = ros::Time::now().toSec();
    }

    /**
     * @brief publishes the control commands to the ROS network and spins.
     */
    void publishCommands(){
        ros::Rate loop_rate(50);
        while (ros::ok())
        {
            geometry_msgs::Twist control_command;

            control_command.linear.x = velocity_command_;
            control_command.linear.y = 0.0;
            control_command.linear.z = 0.0;

            control_command.angular.x = 0.0;
            control_command.angular.y = 0.0;
            control_command.angular.z = ang_velocity_command_;

            command_publisher_.publish(control_command);

            ros::spinOnce();

            loop_rate.sleep();
        }
    }

    /**
     * @brief callback function for the odometry message. Initially the robot is set onto a random motion,
     * then it stops and turns until its heading matches the direction to the goal position, then it is sent forward
     * towards the goal.
     * @param msg: odometry message.
     */
    void setCommand(const nav_msgs::OdometryConstPtr& msg){

        if (first_call_){
            motion_start_time_ = ros::Time::now().toSec();
            first_call_ = false;
        }

        double random_motion_time = 5.0;
        if (ros::Time::now().toSec()-motion_start_time_ <= random_motion_time){
            velocity_command_ = 1.0;
            ang_velocity_command_ = random_angular_speed_;
        }
        else{

            // get robot heading from quaternion
            tf::Pose pose;
            tf::poseMsgToTF(msg->pose.pose, pose);
            // theta_r = atan2(2*(quat_w*quat_z + quat_x*quat_y), 1-2*(quat_y*quat_y+quat_z*quat_z));
            double theta_r = tf::getYaw(pose.getRotation());

            // get robot position
            double x_r = msg->pose.pose.position.x;
            double y_r = msg->pose.pose.position.y;

            // compute angle error
            ang_err_ = atan2(y_goal_-y_r, x_goal_-x_r) - theta_r;
            std::cerr << "theta_r: " << theta_r << " ang_err: " << ang_err_ << " x_r: " << x_r << " y_r: "  <<  y_r << std::endl;
            if(abs(ang_err_) < 0.01){
                turning_finished_ = true;
                at_initial_pos_ = true;
            }

            // stop motion and turn into right heading
            if(!turning_finished_){
                velocity_command_ = 0.0;
                ang_velocity_command_ = controller_gains_.K_P_ang * ang_err_
                                        + controller_gains_.K_I_ang * ang_err_sum_
                                        + controller_gains_.K_D_ang * (ang_err_ - ang_err_prev_);
                // update
                ang_err_prev_ = ang_err_;
                ang_err_sum_ += ang_err_;
            }
            else{
                // get initial position (x_0,y_0)
                if (at_initial_pos_){
                    x_0_ = x_r;
                    y_0_ = y_r;
                    at_initial_pos_ = false;
                }
                ang_velocity_command_ = 0.0;

                // n = (n_x, n_y) is the unit vector pointing from the goal to (x_0,y_0)
                double initial_dist = sqrt((x_0_-x_goal_)*(x_0_-x_goal_) + (y_0_-y_goal_)*(y_0_-y_goal_));
                double n_x = (x_0_ - x_goal_)/initial_dist;
                double n_y = (y_0_ - y_goal_)/initial_dist;

                double signed_distance_to_goal = n_x * (x_r-x_goal_) + n_y * (y_r-y_goal_);
                // apply LQR control law
                velocity_command_ = controller_gains_.K_LQR_vel * signed_distance_to_goal;
            }
        }
    }

    /// variables

    ros::NodeHandle nh_;
    ros::Publisher command_publisher_;
    ros::Subscriber robot_state_subscriber_;

    double x_goal_;
    double y_goal_;
    ControllerGains controller_gains_;

    double velocity_command_{};
    double ang_velocity_command_{};

    double dist_err_{};
    double dist_err_prev_;
    double dist_err_sum_;
    double ang_err_{};
    double ang_err_prev_;
    double ang_err_sum_;

    double random_angular_speed_{};
    bool turning_finished_;
    bool first_call_;
    bool at_initial_pos_;
    double motion_start_time_{};
    double x_0_{};
    double y_0_{};

};
