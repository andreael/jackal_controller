#include "ros/ros.h"
#include "jackal_velocity_publisher/jackal_velocity_publisher.h"

int main(int argc, char **argv)
{
    // init node
    ros::init(argc, argv, "jackal_velocity_publisher");

    ros::NodeHandle n;

    ControllerGains controller_gains;
    n.getParam("K_LQR_vel", controller_gains.K_LQR_vel);
    n.getParam("K_P_ang", controller_gains.K_P_ang);
    n.getParam("K_I_ang", controller_gains.K_I_ang);
    n.getParam("K_D_ang", controller_gains.K_D_ang);

    double x_goal, y_goal;
    n.getParam("x_goal", x_goal);
    n.getParam("y_goal", y_goal);

    // create controller object
    JackalVelocityPublisher jackal_velocity_publisher(n,x_goal,y_goal,controller_gains);

    return 0;
}
